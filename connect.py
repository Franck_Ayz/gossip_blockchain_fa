import sqlite3

try:
    sqliteConnection = sqlite3.connect('database.db')
    cursor = sqliteConnection.cursor()
    print("--------------------------")
    print("Démarrage de SQLite en cours...")
    print("OK")

    with open('.\script\sqlite_create_tables.sql', 'r') as sqlite_file:
        sql_script = sqlite_file.read()

    cursor.executescript(sql_script)
    print("Démarrage du script en cours....")
    print("OK")
    cursor.close()

except sqlite3.Error as error:
    print("Un problème de connection est survenu.", error)
finally:
    if sqliteConnection:
        sqliteConnection.close()
        print("Aurevoir !")
        print("--------------------------")